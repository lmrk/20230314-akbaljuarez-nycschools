
## NYC HighSchool App
This is an iOS app thhat provides info about schools in New York. 

## Description
The app provides a list of all the schools in NY. User can search for a particular school or search by city as well. Once you select the school it will provide a detail view of the school which includes a score dashboard, overview and map of the school.

## Usage
to use the app you can build the app on xcode and deploy it to any prefer ios target.

## Technical
The app is using a MVVM architecture and a Network layer which provides calls to the NYC School API. The structure of the project is devided on: Models, ViewModels, Views and Networking.
