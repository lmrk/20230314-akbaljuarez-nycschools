//
//  NYCSchoolsApp.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
