//
//  HighShoolDetailViewModel.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation
import MapKit

class HighSchoolDetailViewModel: ObservableObject {
    var school: HighSchool {
        return highSchool
    }
    @Published var satScore: SATScore?
    @Published var coordinateRegion: MKCoordinateRegion = MKCoordinateRegion()

    private let highSchool: HighSchool
    
    init(highSchool: HighSchool) {
        self.highSchool = highSchool
        self.coordinateRegion = MKCoordinateRegion(
            center: self.highSchool.coordinate!,
            span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
    }
    
    /// Fetch   SAT Scores for each school when requested
    func fetchSATScores() {
        let dbn = highSchool.id.description
        API.shared.fetchSATScores(for: dbn) { result in
            switch result {
            case .success(let score):
                self.satScore = score
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}


