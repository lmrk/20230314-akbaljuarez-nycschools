//
//  HighSchoolListViewModel.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation

class HighSchoolListViewModel: ObservableObject {
    
    @Published var highSchools = [HighSchool]()
    @Published var selectedCity = ""
    @Published var searchText = ""
    @Published var isLoading = false
    
    private let api: APIClient
    
    /// Compute Filtered High Schools by filtering the searchText
    var filteredHighSchools: [HighSchool] {
        if searchText.isEmpty {
            return highSchools
        } else {
            return highSchools.filter { ($0.schoolName+$0.stateCode+$0.city).lowercased().contains(searchText.lowercased()) }
        }
    }

    init(api: APIClient = API.shared) {
        self.api = api
        fetchHighSchools()
    }
    
    /// Fetch all the high schools
    func fetchHighSchools() {
        isLoading = true
        api.fetchHighSchools { result in
            DispatchQueue.main.async {
                self.isLoading = false
                switch result {
                case .success(let highSchools):
                    self.highSchools = highSchools
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}


