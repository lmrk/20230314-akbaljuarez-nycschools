//
//  SATData.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation

struct SATScore: Codable {
    let dbn: String
    let mathAverage: String
    let criticalReadingAverage: String
    let writingAverage: String
    let numOfSatTestTakers: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case mathAverage = "sat_math_avg_score"
        case criticalReadingAverage = "sat_critical_reading_avg_score"
        case writingAverage = "sat_writing_avg_score"
        case numOfSatTestTakers = "num_of_sat_test_takers"
    }
}
