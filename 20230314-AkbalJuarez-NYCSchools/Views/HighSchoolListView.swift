//
//  HighSchoolListView.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/14/23.
//

import Foundation
import SwiftUI


/// High School List View will show all the schools with its name and city.
struct HighSchoolList: View {
    @ObservedObject var viewModel = HighSchoolListViewModel()

    var body: some View {
        NavigationView {
            VStack {
                if viewModel.isLoading{
                    ZStack{
                        VStack{
                            Spacer()
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: Color.gray))
                                .scaleEffect(2)
                            Spacer()
                        }
                    }
                }
                //Create a List and inclode a NavigationLink to each school to go for details
                List(viewModel.filteredHighSchools, id: \.id) { highSchool in
                    NavigationLink(destination: HighSchoolDetailView(viewModel: HighSchoolDetailViewModel(highSchool: highSchool))) {
                        
                        VStack {
                            HStack{
                                Text(highSchool.schoolName)
                                Spacer()
                            }
                            Spacer()
                            HStack{
                                Text(highSchool.city).font(.caption).foregroundColor(.gray)
                                Spacer()
                            }

                            }
                        .frame(height:65)
                    }
                }.listRowInsets(.init(top:0, leading: 0,bottom: 0, trailing: 0))
            }
            // add a searchable bar wich allow the List to look for the text in the input
            .searchable(text: $viewModel.searchText, placement: .automatic){
            }
            .navigationBarTitle("NYC High Schools")
            .toolbarBackground(Color(red: 160/255, green: 195/255, blue: 255/255), for: .navigationBar)
            .toolbarBackground(.visible, for: .navigationBar)
            //
        }
    }
}
