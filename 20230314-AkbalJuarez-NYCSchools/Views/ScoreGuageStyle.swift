//
//  ScoreGuageStyle.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/15/23.
//

import Foundation
import SwiftUI

struct ScoreGuageStyle: GaugeStyle {
    // we want to use a color that can represent bad top good score, in this case it could be from orange/red to green.
    private var gradient = LinearGradient(gradient: Gradient(colors: [ Color(red: 255/255, green: 160/255, blue: 122/255), Color(red: 124/255, green: 252/255, blue: 0/255) ]), startPoint: .trailing, endPoint: .leading)

    func makeBody(configuration: Configuration) -> some View {
        ZStack {
            //create a background color for the circle
            Circle()
                .foregroundColor(Color(.systemGray6))
            //create the line that will set the value of the gauge
            Circle()
                .trim(from: 0, to: 0.75 * configuration.value)
                .stroke(gradient, lineWidth: 5)
                .rotationEffect(.degrees(135))
            // create the strokes to represent some kind of measure around the circle
            Circle()
                .trim(from: 0, to: 0.75)
                .stroke(Color.black, style: StrokeStyle(lineWidth: 3, lineCap: .butt, lineJoin: .round, dash: [1, 12], dashPhase: 0.0))
                .rotationEffect(.degrees(135))
            //show the labels inside the circle
            VStack {
                configuration.currentValueLabel
                    .font(.system(size: 12, weight: .bold, design: .rounded))
                    .foregroundColor(.gray)
                configuration.label
                    .font(.system(size: 10, design: .rounded))
                    .bold()
                    .foregroundColor(.gray)
            }
 
        }
        .frame(width: 60, height: 60)
 
    }
 
}
