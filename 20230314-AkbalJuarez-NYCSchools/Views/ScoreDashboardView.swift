//
//  ScoreDashboardView.swift
//  20230314-AkbalJuarez-NYCSchools
//
//  Created by Juarez Martinez, Akbal on 3/15/23.
//

import Foundation
import SwiftUI

struct ScoreDashboardView: View {
    let mathAverage:String
    let readingAverage:String
    let writingAverage:String
    let numOfSatTestTakers: String
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("SAT Scores")
                .font(.headline)
            
            HStack(spacing: 20) {
                Spacer()
                //We use the costumized Gauge to display each value of the average score for the school
                Gauge(value: 300,  in: 0...800 ){
                    Text("Math")
                }currentValueLabel: {
                    Text("\(mathAverage)")
                }
                .gaugeStyle(ScoreGuageStyle())
                Spacer()
                
                Gauge(value: 300,  in: 0...400 ){
                    Text("Reading")
                }currentValueLabel: {
                    Text("\(readingAverage)")
                }
                .gaugeStyle(ScoreGuageStyle())
                
                Spacer()
                
                Gauge(value: 300,  in: 0...400 ){
                    Text("Writing")
                }currentValueLabel: {
                    Text("\(writingAverage)")
                }
                .gaugeStyle(ScoreGuageStyle())
                
                Spacer()
                
            }
        }
        Text("Seats takers: \(numOfSatTestTakers)")
            .font(.caption)
    }
}
